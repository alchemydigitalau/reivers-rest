<?php include '_header.php' ?>

<div class="row">
    <?php include '_sidebar.php' ?>

    <div class="col-sm-8">
        
        <h2 class="page-header">Delicious Food</h2>

        <p>All produce used for our breakfasts are sourced locally and we can guarantee the quality, additionally we have our own hens (whom we love dearly and were rescued from a battery farm) and they produce wonderful eggs, many of our guests have been lucky enough to get double yokers, this proves they are happy.</p>

        <p>
            <img src="assets/img/food.jpg" class="img-rounded img-responsive">
        </p>

    </div>

</div>

<?php include '_footer.php'; 