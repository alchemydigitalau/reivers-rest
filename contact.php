<?php include '_header.php' ?>

<div class="row">
    <?php include '_sidebar.php' ?>

    <div class="col-sm-8">
        
        <h2 class="page-header">Contact Us</h2>

        <form role="form">

            <div class="form-group">
                <label>Your Name:</label>
                <input type="text" class="form-control"  >
            </div>

            <div class="form-group">
                <label>Your Email Address:</label>
                <input type="email" class="form-control" >
            </div>

            <div class="form-group">
                <label>Your Telephone Number:</label>
                <input type="text" class="form-control" >
            </div>

            <div class="form-group">
                <label>Your Message:</label>
                <textarea class="form-control" rows="6"></textarea>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>

    </div>

</div>

<?php include '_footer.php'; 