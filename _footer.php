            </div>
        </main>

        <footer class="container">
            &copy; <?php echo date('Y'); ?> The Bed Booker Ltd. | <a href="http://demo.bookyourbeds.com/the-reivers-rest/signin">Visit the admin back-office</a>
            
        </footer>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

        <script type="text/javascript" src="assets/js/compressed/picker.js"></script>
        <script type="text/javascript" src="assets/js/compressed/picker.date.js"></script>
        <script type="text/javascript" src="assets/js/compressed/legacy.js"></script>

        <script type="text/javascript">
        <!--
        $(document).ready(function()
        {
            $('.datepicker').pickadate(
                {
                    formatSubmit: 'dd/mm/yyyy',
                    hiddenName: true
                });
        });
        -->
        </script>
    </body>
</html>