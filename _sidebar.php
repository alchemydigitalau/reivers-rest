<div class="col-sm-4">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Make a Booking</h3>
        </div>

        <form action="http://demo.bookyourbeds.com/the-reivers-rest/salesdesk/search#results" method="POST" role="form" class="panel-body">

            <div class="row">
                <div class="form-group col-sm-8">
                    <label>Arrival date:</label>
                    <input type="text" class="form-control datepicker" name="start_at" >
                </div>

                <div class="form-group col-sm-6">
                    <label>No. of Guests:</label>
                    <select class="form-control" name="guests">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>6</option>
                    </select>
                </div>

                <div class="form-group col-sm-6">
                    <label>Duration:</label>
                    <select class="form-control" name="duration">
                        <option>1 night</option>
                        <option>2 nights</option>
                        <option>3 nights</option>
                        <option>4 nights</option>
                        <option>5 nights</option>
                    </select>
                </div>
            </div>

            <button type="submit" class="btn btn-primary btn-block">Search Availability</button>


        </form>

        <div class="panel-footer">
            <strong>Please note:</strong> This is a fictional site to demonstrate ways to integrate with <a href="http://bookyourbeds.com">BookYourBeds</a>. Bookings will not be honoured.
        </div>
    </div>

    <div class="alert alert-success">
        <strong><a href="http://demo.bookyourbeds.com/the-reivers-rest/signin">Visit the admin back-office</a></strong> to see the booking system in action.
    </div>


</div>