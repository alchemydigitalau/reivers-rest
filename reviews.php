<?php include '_header.php' ?>

<div class="row">
    <?php include '_sidebar.php' ?>

    <div class="col-sm-8">
        
        <h2 class="page-header">Wonderful Reviews</h2>

        <p>The Reiver's Rest has welcomed guests from all over the world and if you don’t believe us just take a look at our Trip Advisor reviews or read some our best comments below:</p>

        <blockquote>
            <p>"Best nights sleep in years and the breakfast was to die for... My wife and I will definitely be back"
            <footer>Barry and Debbie, Kansas</footer>
        </blockquote>

        <blockquote>
            <p>"Such a lovely warm welcome, Dave and Helen couldn’t do enough for us, a truly special place."
            <footer>Bill and Lena, Derbyshire</footer>
        </blockquote>

        <blockquote>
            <p>"The Reivers Rest is a hidden treasure"
            <footer>Tamsyn and Phil, Queensland, Australia</footer>
        </blockquote>

        <p>Many have already enjoyed our hospitality and we hope that you will to. </p>

    </div>

</div>

<?php include '_footer.php'; 