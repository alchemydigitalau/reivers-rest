<?php include '_header.php' ?>

<div class="row">
    <?php include '_sidebar.php' ?>

    <div class="col-sm-8">
        
        <h2 class="page-header">Welcome to The Reiver's Rest</h2>

        <p>The Reiver’s Rest is our 4 Star, eco friendly, award winning, cosy and comfortable, Bed and Breakfast. This former farmer’s cottage is set in the beautiful Scottish Borders with stunning South facing views from our guest decking area right across the famous river Tweed and beyond onto the rolling Cheviot Hills. </p>

        <p>
            <img src="assets/img/home.jpg" class="img-rounded img-responsive">
        </p>

    </div>

</div>

<?php include '_footer.php'; 