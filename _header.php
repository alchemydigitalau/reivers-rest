<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>The Reiver's Rest</title>

        <!-- Bootstrap -->
        <link href="assets/css/site.css" rel="stylesheet">

        <link href="assets/js/compressed/themes/default.css" rel="stylesheet">
        <link href="assets/js/compressed/themes/default.date.css" rel="stylesheet">

        <link href='http://fonts.googleapis.com/css?family=Courgette|Lato:400,900' rel='stylesheet' type='text/css'>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="alert text-center pinned">
            <strong>Please note:</strong> This is a fictional site to demonstrate ways to integrate with <a href="http://bookyourbeds.com">BookYourBeds</a>. Bookings will not be honoured.
        </div>

        <header>
            <div class="overlay">
                <div class="container">

                    <nav class="navbar" role="navigation">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            
                            <a class="navbar-brand" href="/">The Reiver's<br />Rest</a>
                        </div>

                        <div class="collapse navbar-collapse" id="navbar-collapse">
                            <ul class="nav navbar-nav pull-right">
                                <li><a href="/rooms.php">Rooms</a></li>
                                <li><a href="/food.php">Food</a></li>
                                <li><a href="/reviews.php">Reviews</a></li>
                                <li><a href="/gallery.php">Gallery</a></li>
                                <li><a href="/contact.php">Contact Us</a></li>
                                <li><a href="http://demo.bookyourbeds.com/the-reivers-rest">Availability</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>

                <div class="container">
                    <h1 class="text-center">A fantastic B&amp;B in the heart of the Scottish Borders</h1>
                </div>
            </div>

            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
              </ol>

              <!-- Wrapper for slides -->
              <div class="carousel-inner">
                <div class="item active" style="background-image: url('/assets/img/slide-1.jpg');">
                </div>

                <div class="item" style="background-image: url('/assets/img/slide-2.jpg');">
                </div>

                <div class="item" style="background-image: url('/assets/img/slide-3.jpg');">
                </div>
              </div>

            </div>

        </header>

        <main>
            <div class="container">