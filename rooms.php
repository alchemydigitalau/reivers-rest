<?php include '_header.php' ?>

<div class="row">
    <?php include '_sidebar.php' ?>

    <div class="col-sm-8">
        
        <h2 class="page-header">Our Rooms</h2>

        <p>Our rooms are all en-suite with power showers. Don’t worry if you think that isn’t very eco friendly because the water is reused. Our beds are King-size with double sprung mattresses which give amazing support and provide you with a very comfortable and peaceful nights sleep. The rooms have all the usual amenities that you expect in a modern bed and breakfast from Sky television to hair driers, plus all rooms also have free WiFi access, allowing you to update your friends and family on how relaxed you are. </p>

        <p>
            <img src="assets/img/rooms.jpg" class="img-rounded img-responsive">
        </p>

    </div>

</div>

<?php include '_footer.php'; 