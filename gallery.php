<?php include '_header.php' ?>

<div class="row">
    <?php include '_sidebar.php' ?>

    <div class="col-sm-8">
        
        <h2 class="page-header">Image Gallery</h2>

        <div class="row">
            <div class="col-xs-6 col-md-3">
                <a href="#" class="thumbnail">
                    <img src="assets/img/thumb-1.jpg" alt="...">
                </a>
            </div>

            <div class="col-xs-6 col-md-3">
                <a href="#" class="thumbnail">
                    <img src="assets/img/thumb-2.jpg" alt="...">
                </a>
            </div>

            <div class="col-xs-6 col-md-3">
                <a href="#" class="thumbnail">
                    <img src="assets/img/thumb-3.jpg" alt="...">
                </a>
            </div>

            <div class="col-xs-6 col-md-3">
                <a href="#" class="thumbnail">
                    <img src="assets/img/thumb-4.jpg" alt="...">
                </a>
            </div>


            <div class="col-xs-6 col-md-3">
                <a href="#" class="thumbnail">
                    <img src="assets/img/thumb-5.jpg" alt="...">
                </a>
            </div>

            <div class="col-xs-6 col-md-3">
                <a href="#" class="thumbnail">
                    <img src="assets/img/thumb-6.jpg" alt="...">
                </a>
            </div>

            <div class="col-xs-6 col-md-3">
                <a href="#" class="thumbnail">
                    <img src="assets/img/thumb-7.jpg" alt="...">
                </a>
            </div>

            <div class="col-xs-6 col-md-3">
                <a href="#" class="thumbnail">
                    <img src="assets/img/thumb-8.jpg" alt="...">
                </a>
            </div>

        </div>

    </div>

</div>

<?php include '_footer.php'; 